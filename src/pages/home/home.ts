import {
  Component,
  OnInit
} from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup } from '@angular/forms';

import { HomeService } from './home.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  public map: google.maps.Map;
  public gmapSetup: any;
  public loadingGeolocationFlag: boolean;
  public submitFlag: boolean;
  public userForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public homeService: HomeService
  ) {
    this.submitFlag = false;
    this.gmapSetup = {
      zoom: 4,
      latitude: 39.8282,
      longitude: -98.5795, // Default: USA
      scrollWheel: true
    };
    this.loadingGeolocationFlag = false;
  }

  public ngOnInit() {
    this.userForm = this.homeService.initUserForm();
  }

  public formSubmit(event, isValid): void {
    this.submitFlag = true;

    if (isValid) {
      alert('Data is valid.');
    }
  }

  public setGeolocation(): void {
    this.loadingGeolocationFlag = true;
    this.homeService.setCurrentLocation(this.gmapSetup).then(
      () => {
        this.loadingGeolocationFlag = false;
      },
      () => {
        this.loadingGeolocationFlag = false;
        alert('Your location could not be retrieved.');
      }
    );
  }

}
