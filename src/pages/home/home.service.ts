import {Injectable} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {Camera} from '@ionic-native/camera';
import {File} from '@ionic-native/file';
import {FilePath} from '@ionic-native/file-path';
import {Geolocation} from '@ionic-native/geolocation';
import {
  ActionSheetController,
  Platform,
  normalizeURL
} from 'ionic-angular';

import {User} from './user.model';

declare var cordova: any;

@Injectable()
export class HomeService {

  public lastImage: string;

  constructor(
    private formBuilder: FormBuilder,
    private platform: Platform,
    private actionSheetController: ActionSheetController,
    private camera: Camera,
    private file: File,
    private filePath: FilePath,
    private geolocation: Geolocation
  ) {
    this.lastImage = 'assets/imgs/user-default.png';
  }

  public initUserForm(): FormGroup {
    const newUser = new User;

    return this.formBuilder.group({
      userName: [newUser.name, <any>Validators.required],
      userEmail: [newUser.email, [<any>Validators.required, <any>Validators.email]],
      userBio: [newUser.bio]
    });
  }

  public setCurrentLocation(gmapSetup: any,): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        if (this.geolocation) {
          navigator.geolocation.getCurrentPosition(
            (position) => {
              gmapSetup.latitude = position.coords.latitude;
              gmapSetup.longitude = position.coords.longitude;
              gmapSetup.zoom = 12;
              resolve();
            },
            () => {
              reject();
            },
            {timeout: 9000})
        } else {
          reject();
        }
      }
    );
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetController.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY).then(
              (val) => {
                this.lastImage = normalizeURL(val);
              }
            );

          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA).then(
              (val) => {
                this.lastImage = normalizeURL(val);
              }
            );
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  private takePicture(sourceType, extension?): Promise<any> {
    let encodingType;

    if (extension) {
      encodingType = extension;
    } else {
      encodingType = 0; // 0 = jpg, 1 = png
    }

    return new Promise(
      (resolve) => {
        let options = {
          quality: 25,
          sourceType: sourceType,
          encodingType: encodingType,
          saveToPhotoAlbum: false,
          correctOrientation: true
        };

        this.camera.getPicture(options).then((imagePath) => {
          if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath).then(filePath => {
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
              let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));

              this.copyFileToLocalDir(correctPath, currentName, this.createFileName(encodingType)).then(
                (pathForImage) => {
                  resolve(pathForImage);
                }
              );
            });
          } else {
            let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

            this.copyFileToLocalDir(correctPath, currentName, this.createFileName(encodingType)).then(
              (pathForImage) => {
                resolve(pathForImage);
              }
            );
          }
        }, (err) => {
          alert('Error while selecting image.');
          return null;
        });
      }
    );
  }

  private copyFileToLocalDir(namePath, currentName, newFileName): Promise<any> {
    return new Promise((resolve, reject) => {
      this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then((success) => {
        this.lastImage = newFileName;
        let pathForImage = this.pathForImage(newFileName);
        resolve(pathForImage);
      }, (error) => {
        alert('Error while storing file.');
        reject(error);
      });
    });
  }

  private pathForImage(img): string {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  private createFileName(type) {
    let filetype: string;
    switch (type) {
      case 0:
        filetype = '.jpg';
        break;
      case 1:
        filetype = '.png';
        break;
      default:
        filetype = '.jpg';
    }

    let d = new Date(),
      n = d.getTime(),
      newFileName = n + filetype;
    return newFileName;
  }

}
