interface userContract {
  name: string;
  email: string;
  gender: string;
  bio: string;
}

export class User implements userContract {
  constructor (
    public name = null,
    public email = null,
    public gender = null,
    public bio = null
  ) {}
}
